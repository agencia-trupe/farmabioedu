<?php

namespace App\Http\Controllers\CadastroAuth;

use App\Models\Cadastro;
use Auth;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;
use App\Http\Requests\CadastrosRequest;

class AuthController extends Controller
{
    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    protected $redirectTo   = '/';
    protected $guard        = 'cadastro';
    protected $loginView    = 'frontend.login';
    protected $registerView = 'frontend.cadastro';

    public function __construct()
    {
        $this->middleware('guest.cadastro', ['except' => 'logout']);
    }

    protected function create(CadastrosRequest $request)
    {
        $input = $request->except('senha_confirmation');
        $input['senha'] = bcrypt($request->get('senha'));

        $usuario = Cadastro::create($input);

        Auth::guard('cadastro')->loginUsingId($usuario->id);

        return redirect()->route('home');
    }

    protected function login(Request $request) {
        if (Auth::guard('cadastro')->attempt([
            'email'    => $request->get('email'),
            'password' => $request->get('senha')
        ])) {
            return redirect()->route('home');
        } else {
            return redirect()->route('loginCadastro')->withInput()->with('erro-login', 'e-mail ou senha inválidos');
        }
    }
}
