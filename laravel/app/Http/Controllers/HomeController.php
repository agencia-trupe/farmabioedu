<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\CadastrosRequest;

use App\Models\SobreOPrograma;
use App\Models\LinksUteis;
use App\Models\ReferenciasBibliograficas;
use App\Models\Glossario;
use App\Models\PoliticaDePrivacidade;
use App\Models\Palestrante;
use App\Models\Curso;
use App\Models\Conselho;
use App\Models\Aula;
use App\Models\CadastroQuestaoResposta;

class HomeController extends Controller
{
    public function index()
    {
        $palestrantes = Palestrante::ordenados()->get();

        return view('frontend.home', compact('palestrantes'));
    }

    public function sobre()
    {
        $texto = SobreOPrograma::first();
        $texto->titulo = 'Sobre o Programa';
        $texto->icone  = 'sobre';

        return view('frontend.texto', compact('texto'));
    }

    public function aulas(Aula $aula)
    {
        $user  = auth('cadastro')->user();
        $aulas = Aula::with('palestrante')->ordenados()->get();

        if (! $aula->exists) $aula = $aulas->first();
        if (! $aula) abort('404');

        foreach ($aulas as $key => $a) {
            if ($a->id == $aula->id) $numero = $key;
        }

        return view('frontend.aulas', compact('user', 'aulas', 'aula', 'numero'));
    }

    public function aulaAssistida($id)
    {
        if (! request()->ajax()) {
            return response('Acesso não autorizado.', 401);
        }

        $aula = Aula::findOrFail($id);
        $user = auth('cadastro')->user();

        if (! $user->aulas->contains($id)) {
            $user->aulas()->attach($id);
        }
    }

    public function aulaQuestaoPost(Request $request, $aulaId, $questaoId)
    {
        if (! request()->ajax()) {
            return response('Acesso não autorizado.', 401);
        }

        $this->validate($request, [
            'alternativa' => 'required|integer'
        ]);

        $aula        = Aula::findOrFail($aulaId);
        $questao     = $aula->questoes()->findOrFail($questaoId);
        $alternativa = $questao->alternativas()->findOrFail(request('alternativa'));

        $user = auth('cadastro')->user();

        if ($user->respostaQuestao($questao->id)) {
            return response()->json([
                'erro' => 'Erro: Essa questão já foi respondida.'
            ]);
        }

        $user->respostas()->create([
            'questao_id'     => $questao->id,
            'alternativa_id' => $alternativa->id
        ]);

        return response()->json([
            'selecao'  => (int)request('alternativa'),
            'corretas' => collect($questao->alternativas()
                ->where('alternativa_correta', true)
                ->lists('id'))->map(function($id) {
                    return (int)$id;
                })
        ]);
    }

    public function palestrantes()
    {
        $palestrantes = Palestrante::ordenados()->get();

        return view('frontend.palestrantes', compact('palestrantes'));
    }

    public function calendario()
    {
        $cursos = Curso::ordenados()->get();

        return view('frontend.calendario', compact('cursos'));
    }

    public function conselhos()
    {
        $conselhos = Conselho::ordenados()->get();

        return view('frontend.conselhos', compact('conselhos'));
    }

    public function links()
    {
        $texto = LinksUteis::first();
        $texto->titulo = 'Links Úteis';

        return view('frontend.texto', compact('texto'));
    }

    public function referencias()
    {
        $texto = ReferenciasBibliograficas::first();
        $texto->titulo = 'Referências Bibliográficas';

        return view('frontend.texto', compact('texto'));
    }

    public function glossario()
    {
        $texto = Glossario::first();
        $texto->titulo = 'Glossário';

        return view('frontend.texto', compact('texto'));
    }

    public function politica()
    {
        $texto = PoliticaDePrivacidade::first();
        $texto->titulo = 'Política de Privacidade';

        return view('frontend.texto', compact('texto'));
    }

    public function dados()
    {
        $user = auth('cadastro')->user();

        return view('frontend.dados-cadastrais', compact('user'));
    }

    public function dadosPost(CadastrosRequest $request)
    {
        $input = array_filter($request->except('senha_confirmation'), 'strlen');

        if (isset($input['senha'])) {
            $input['senha'] = bcrypt($input['senha']);
        }

        auth('cadastro')->user()->update($input);

        return redirect()->back()->with('success', 'Cadastro alterado com sucesso!');
    }
}
