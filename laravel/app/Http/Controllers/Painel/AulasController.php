<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\AulasRequest;
use App\Http\Controllers\Controller;

use App\Models\Aula;
use App\Models\Palestrante;

class AulasController extends Controller
{
    public function index()
    {
        $registros = Aula::ordenados()->get();

        return view('painel.aulas.index', compact('registros'));
    }

    public function create()
    {
        $palestrantes = Palestrante::ordenados()->lists('nome', 'id');

        return view('painel.aulas.create', compact('palestrantes'));
    }

    public function store(AulasRequest $request)
    {
        try {

            $input = $request->all();

            Aula::create($input);

            return redirect()->route('painel.aulas.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Aula $registro)
    {
        $palestrantes = Palestrante::ordenados()->lists('nome', 'id');

        return view('painel.aulas.edit', compact('registro', 'palestrantes'));
    }

    public function update(AulasRequest $request, Aula $registro)
    {
        try {

            $input = $request->all();

            $registro->update($input);

            return redirect()->route('painel.aulas.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Aula $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.aulas.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
