<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\VideosRequest;
use App\Http\Controllers\Controller;

use App\Models\Cadastro;
use App\Models\Aula;

use Maatwebsite\Excel\Facades\Excel;

class CadastrosController extends Controller
{
    public function index()
    {
        $registros = Cadastro::with('aulas')->paginate(10);

        return view('painel.cadastros.index', compact('registros'));
    }

    public function show(Cadastro $registro)
    {
        $aulas = Aula::ordenados()->get();

        return view('painel.cadastros.show', compact('registro', 'aulas'));
    }

    public function exportar()
    {
        $fileName = 'FarmaBioEdu-Cadastros_'.date('d-m-Y-H-i');
        $data     = Cadastro::with('aulas')->get()->map(function($c) {
            return [
                'Nome'                  => $c->nome,
                'E-mail'                => $c->email,
                'Registro Profissional' => $c->registro_profissional,
                'Profissão'             => $c->profissao,
                'Aulas Concluídas'      => "$c->fracaoAulasConcluidas ($c->porcentagemAulasConcluidas%)",
                'Aulas'                 => $c->aulas->pluck('titulo')->implode("\n")
            ];
        });

        Excel::create($fileName, function ($excel) use ($data) {
            $excel->sheet('cadastros', function ($sheet) use ($data) {
                $sheet->fromModel($data);
                $sheet->row(1, function($row) {
                    $row->setBackground('#eeeeee');
                });
                $sheet->getStyle('A1:M1')->applyFromArray([
                    'font' => ['bold' => true],
                ]);
                $sheet->getDefaultStyle()
                    ->getAlignment()
                    ->applyFromArray([
                        'horizontal'   	 => \PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                        'vertical'     	 => \PHPExcel_Style_Alignment::VERTICAL_TOP,
                        'wrap'           => true
                    ]);
            });
        })->download('xls');
    }
}
