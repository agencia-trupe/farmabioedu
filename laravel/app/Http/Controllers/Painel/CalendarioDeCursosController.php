<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\CalendarioDeCursosRequest;
use App\Http\Controllers\Controller;

use App\Models\Curso;

class CalendarioDeCursosController extends Controller
{
    public function index()
    {
        $registros = Curso::ordenados()->get();

        return view('painel.calendario-de-cursos.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.calendario-de-cursos.create');
    }

    public function store(CalendarioDeCursosRequest $request)
    {
        try {

            $input = $request->all();

            Curso::create($input);

            return redirect()->route('painel.calendario-de-cursos.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Curso $registro)
    {
        return view('painel.calendario-de-cursos.edit', compact('registro'));
    }

    public function update(CalendarioDeCursosRequest $request, Curso $registro)
    {
        try {

            $input = $request->all();

            $registro->update($input);

            return redirect()->route('painel.calendario-de-cursos.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Curso $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.calendario-de-cursos.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
