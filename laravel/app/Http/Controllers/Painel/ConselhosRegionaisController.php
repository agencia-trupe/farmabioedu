<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ConselhosRegionaisRequest;
use App\Http\Controllers\Controller;

use App\Models\Conselho;

class ConselhosRegionaisController extends Controller
{
    public function index()
    {
        $registros = Conselho::ordenados()->get();

        return view('painel.conselhos-regionais.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.conselhos-regionais.create');
    }

    public function store(ConselhosRegionaisRequest $request)
    {
        try {

            $input = $request->all();

            Conselho::create($input);

            return redirect()->route('painel.conselhos-regionais.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Conselho $registro)
    {
        return view('painel.conselhos-regionais.edit', compact('registro'));
    }

    public function update(ConselhosRegionaisRequest $request, Conselho $registro)
    {
        try {

            $input = $request->all();

            $registro->update($input);

            return redirect()->route('painel.conselhos-regionais.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Conselho $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.conselhos-regionais.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
