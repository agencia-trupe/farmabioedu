<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\GlossarioRequest;
use App\Http\Controllers\Controller;

use App\Models\Glossario;

class GlossarioController extends Controller
{
    public function index()
    {
        $registro = Glossario::first();

        return view('painel.glossario.edit', compact('registro'));
    }

    public function update(GlossarioRequest $request, Glossario $registro)
    {
        try {
            $input = $request->all();


            $registro->update($input);

            return redirect()->route('painel.glossario.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
