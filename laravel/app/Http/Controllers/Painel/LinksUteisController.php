<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\LinksUteisRequest;
use App\Http\Controllers\Controller;

use App\Models\LinksUteis;

class LinksUteisController extends Controller
{
    public function index()
    {
        $registro = LinksUteis::first();

        return view('painel.links-uteis.edit', compact('registro'));
    }

    public function update(LinksUteisRequest $request, LinksUteis $registro)
    {
        try {
            $input = $request->all();


            $registro->update($input);

            return redirect()->route('painel.links-uteis.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
