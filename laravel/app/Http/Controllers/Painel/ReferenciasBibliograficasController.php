<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ReferenciasBibliograficasRequest;
use App\Http\Controllers\Controller;

use App\Models\ReferenciasBibliograficas;

class ReferenciasBibliograficasController extends Controller
{
    public function index()
    {
        $registro = ReferenciasBibliograficas::first();

        return view('painel.referencias-bibliograficas.edit', compact('registro'));
    }

    public function update(ReferenciasBibliograficasRequest $request, ReferenciasBibliograficas $registro)
    {
        try {
            $input = $request->all();


            $registro->update($input);

            return redirect()->route('painel.referencias-bibliograficas.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
