<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AulasQuestoesRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'questao' => 'required',
        ];

        if ($this->method() != 'POST') {
        }

        return $rules;
    }

    public function attributes()
    {
        return [
            'questao' => 'questão'
        ];
    }
}
