<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AulasRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'palestrante_id' => 'required',
            'titulo' => 'required',
            'descricao' => 'required',
            'video' => 'required',
        ];

        if ($this->method() != 'POST') {
        }

        return $rules;
    }

    public function attributes()
    {
        return [
            'palestrante_id' => 'palestrante'
        ];
    }
}
