<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CalendarioDeCursosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'data' => 'required',
            'titulo' => 'required',
            'informacoes' => 'required',
        ];

        if ($this->method() != 'POST') {
        }

        return $rules;
    }
}
