<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ConselhosRegionaisRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'titulo' => 'required',
            'informacoes' => 'required',
        ];

        if ($this->method() != 'POST') {
        }

        return $rules;
    }
}
