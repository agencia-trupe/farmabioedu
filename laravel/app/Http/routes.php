<?php

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('sobre-o-programa', 'HomeController@sobre')->name('sobre');
    Route::get('politica-de-privacidade', 'HomeController@politica')->name('politica');

    // Páginas restritas
    Route::group(['middleware' => ['auth.cadastro']], function() {
        Route::get('dados-cadastrais', 'HomeController@dados')->name('dados');
        Route::patch('dados-cadastrais', 'HomeController@dadosPost')->name('dadosPost');
        Route::get('aulas/{aula_slug?}', 'HomeController@aulas')->name('aulas');
        Route::get('aulas/{aula_id}/assistida', 'HomeController@aulaAssistida')->name('aulas.assistida');
        Route::post('aulas/{aula_id}/questao/{questao_id}', 'HomeController@aulaQuestaoPost')->name('aulas.questaoPost');
        Route::get('palestrantes', 'HomeController@palestrantes')->name('palestrantes');
        Route::get('calendario-de-cursos', 'HomeController@calendario')->name('calendario');
        Route::get('conselhos-regionais', 'HomeController@conselhos')->name('conselhos');
        Route::get('contato', 'ContatoController@index')->name('contato');
        Route::post('contato', 'ContatoController@post')->name('contato.post');
        Route::get('links-uteis', 'HomeController@links')->name('links');
        Route::get('referencias-bibliograficas', 'HomeController@referencias')->name('referencias');
        Route::get('glossario', 'HomeController@glossario')->name('glossario');
    });

    // Login/Cadastro
    Route::get('login', 'CadastroAuth\AuthController@showLoginForm')->name('loginCadastro');
    Route::post('login', 'CadastroAuth\AuthController@login')->name('loginCadastroPost');
    Route::get('cadastro', 'CadastroAuth\AuthController@showRegistrationForm')->name('cadastro');
    Route::post('cadastro', 'CadastroAuth\AuthController@create')->name('cadastroPost');

    // Redefinição de Senha
    Route::get('esqueci-minha-senha', 'CadastroAuth\PasswordController@showLinkRequestForm')->name('esqueci');
    Route::post('esqueci-minha-senha', 'CadastroAuth\PasswordController@sendResetLinkEmail')->name('redefinicao');
    Route::get('redefinicao-de-senha/{token}', 'CadastroAuth\PasswordController@showResetForm')->name('redefinirForm');
    Route::post('redefinicao-de-senha', 'CadastroAuth\PasswordController@reset')->name('redefinir');

    // Logout
    Route::get('logout', 'CadastroAuth\AuthController@logout')->name('logoutCadastro');


    // Painel
    Route::group([
        'prefix'     => 'painel',
        'namespace'  => 'Painel',
        'middleware' => ['auth']
    ], function() {
        Route::get('/', 'PainelController@index')->name('painel');

        /* GENERATED ROUTES */
        Route::get('cadastros/exportar', 'CadastrosController@exportar')->name('painel.cadastros.exportar');
        Route::resource('cadastros', 'CadastrosController');
		Route::resource('aulas', 'AulasController');
		Route::resource('aulas.questoes', 'AulasQuestoesController');
		Route::resource('aulas.questoes.alternativas', 'AulasQuestoesAlternativasController');
		Route::resource('palestrantes', 'PalestrantesController');
		Route::resource('calendario-de-cursos', 'CalendarioDeCursosController');
		Route::resource('conselhos-regionais', 'ConselhosRegionaisController');
		Route::resource('sobre-o-programa', 'SobreOProgramaController', ['only' => ['index', 'update']]);
		Route::resource('links-uteis', 'LinksUteisController', ['only' => ['index', 'update']]);
		Route::resource('referencias-bibliograficas', 'ReferenciasBibliograficasController', ['only' => ['index', 'update']]);
		Route::resource('glossario', 'GlossarioController', ['only' => ['index', 'update']]);
		Route::resource('politica-de-privacidade', 'PoliticaDePrivacidadeController', ['only' => ['index', 'update']]);
		Route::resource('configuracoes', 'ConfiguracoesController', ['only' => ['index', 'update']]);

        Route::get('contato/recebidos/{recebidos}/toggle', ['as' => 'painel.contato.recebidos.toggle', 'uses' => 'ContatosRecebidosController@toggle']);
        Route::resource('contato/recebidos', 'ContatosRecebidosController');
        Route::resource('contato', 'ContatoController');
        Route::resource('usuarios', 'UsuariosController');

        Route::post('ckeditor-upload', 'PainelController@imageUpload');
        Route::post('order', 'PainelController@order');
        Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

        Route::get('generator', 'GeneratorController@index')->name('generator.index');
        Route::post('generator', 'GeneratorController@submit')->name('generator.submit');
    });

    // Auth
    Route::group([
        'prefix'    => 'painel',
        'namespace' => 'Auth'
    ], function() {
        Route::get('login', 'AuthController@showLoginForm')->name('auth');
        Route::post('login', 'AuthController@login')->name('login');
        Route::get('logout', 'AuthController@logout')->name('logout');
    });
});
