<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

use Illuminate\Support\Facades\Cache;

class Aula extends Model implements SluggableInterface
{
    use SluggableTrait;

    protected $sluggable = [
        'build_from' => 'titulo',
        'save_to'    => 'slug',
        'on_update'  => true
    ];

    protected $table = 'aulas';

    protected $guarded = ['id'];

    public static function boot()
    {
        parent::boot();

        self::created(function($model) {
            Cache::forget('aulas_count');
            return static::cachedCount();
        });

        self::deleted(function($model) {
            Cache::forget('aulas_count');
            return static::cachedCount();
        });
    }

    public static function cachedCount()
    {
        return Cache::rememberForever('aulas_count', function () {
            return static::count();
        });
    }

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function palestrante()
    {
        return $this->belongsTo(Palestrante::class, 'palestrante_id');
    }

    public function questoes()
    {
        return $this->hasMany(AulaQuestao::class, 'aula_id')->with('alternativas')->ordenados();
    }
}
