<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;
use Carbon\Carbon;

class Curso extends Model
{
    protected $table = 'calendario_de_cursos';

    protected $guarded = ['id'];

    protected $dates = ['data'];

    public function setDataAttribute($value)
    {
        $this->attributes['data'] = Carbon::createFromFormat('!d/m/Y', $value);
    }

    public function scopeOrdenados($query)
    {
        return $query->orderBy('data', 'ASC')->orderBy('id', 'ASC');
    }
}
