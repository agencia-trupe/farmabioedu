<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Glossario extends Model
{
    protected $table = 'glossario';

    protected $guarded = ['id'];

}
