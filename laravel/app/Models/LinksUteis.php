<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class LinksUteis extends Model
{
    protected $table = 'links_uteis';

    protected $guarded = ['id'];

}
