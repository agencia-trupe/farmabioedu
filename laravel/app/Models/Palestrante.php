<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Palestrante extends Model
{
    protected $table = 'palestrantes';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public static function upload_foto()
    {
        return CropImage::make('foto', [
            'width'  => 120,
            'height' => 120,
            'path'   => 'assets/img/palestrantes/'
        ]);
    }

    public function aulas()
    {
        return $this->hasMany(Aula::class, 'palestrante_id');
    }
}
