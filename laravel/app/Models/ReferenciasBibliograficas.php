<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class ReferenciasBibliograficas extends Model
{
    protected $table = 'referencias_bibliograficas';

    protected $guarded = ['id'];

}
