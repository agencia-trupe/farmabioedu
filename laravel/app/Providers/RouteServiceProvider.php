<?php

namespace App\Providers;

use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function boot(Router $router)
    {
        $router->model('cadastros', 'App\Models\Cadastro');
		$router->model('aulas', 'App\Models\Aula');
		$router->model('questoes', 'App\Models\AulaQuestao');
		$router->model('alternativas', 'App\Models\AulaQuestaoAlternativa');
		$router->model('palestrantes', 'App\Models\Palestrante');
		$router->model('calendario-de-cursos', 'App\Models\Curso');
		$router->model('conselhos-regionais', 'App\Models\Conselho');
		$router->model('sobre-o-programa', 'App\Models\SobreOPrograma');
		$router->model('links-uteis', 'App\Models\LinksUteis');
		$router->model('referencias-bibliograficas', 'App\Models\ReferenciasBibliograficas');
		$router->model('glossario', 'App\Models\Glossario');
		$router->model('politica-de-privacidade', 'App\Models\PoliticaDePrivacidade');
		$router->model('configuracoes', 'App\Models\Configuracoes');
        $router->model('recebidos', 'App\Models\ContatoRecebido');
        $router->model('contato', 'App\Models\Contato');
        $router->model('usuarios', 'App\Models\User');

        $router->bind('aula_slug', function($key) {
            return \App\Models\Aula::whereSlug($key)->firstOrFail();
        });

        parent::boot($router);
    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function map(Router $router)
    {
        $router->group(['namespace' => $this->namespace], function ($router) {
            require app_path('Http/routes.php');
        });
    }
}
