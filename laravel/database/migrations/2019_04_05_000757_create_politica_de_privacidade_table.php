<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePoliticaDePrivacidadeTable extends Migration
{
    public function up()
    {
        Schema::create('politica_de_privacidade', function (Blueprint $table) {
            $table->increments('id');
            $table->text('texto');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('politica_de_privacidade');
    }
}
