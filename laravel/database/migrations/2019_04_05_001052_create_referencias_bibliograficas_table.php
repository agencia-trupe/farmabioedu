<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReferenciasBibliograficasTable extends Migration
{
    public function up()
    {
        Schema::create('referencias_bibliograficas', function (Blueprint $table) {
            $table->increments('id');
            $table->text('texto');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('referencias_bibliograficas');
    }
}
