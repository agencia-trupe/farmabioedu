<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSobreOProgramaTable extends Migration
{
    public function up()
    {
        Schema::create('sobre_o_programa', function (Blueprint $table) {
            $table->increments('id');
            $table->text('texto');
            $table->string('video');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('sobre_o_programa');
    }
}
