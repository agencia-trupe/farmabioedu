<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConselhosRegionaisTable extends Migration
{
    public function up()
    {
        Schema::create('conselhos_regionais', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('titulo');
            $table->text('informacoes');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('conselhos_regionais');
    }
}
