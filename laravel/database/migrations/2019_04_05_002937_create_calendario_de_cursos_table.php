<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCalendarioDeCursosTable extends Migration
{
    public function up()
    {
        Schema::create('calendario_de_cursos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('data');
            $table->string('titulo');
            $table->text('informacoes');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('calendario_de_cursos');
    }
}
