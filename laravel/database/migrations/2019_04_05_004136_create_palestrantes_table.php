<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePalestrantesTable extends Migration
{
    public function up()
    {
        Schema::create('palestrantes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('nome');
            $table->text('apresentacao');
            $table->text('curriculo');
            $table->string('foto');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('palestrantes');
    }
}
