<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCadastroQuestaoRespostaTable extends Migration
{
    public function up()
    {
        Schema::create('cadastro_questao_resposta', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cadastro_id')->unsigned();
            $table->foreign('cadastro_id')->references('id')->on('cadastros')->onDelete('cascade');
            $table->integer('questao_id')->unsigned();
            $table->foreign('questao_id')->references('id')->on('aulas_questoes')->onDelete('cascade');
            $table->integer('alternativa_id')->unsigned();
            $table->foreign('alternativa_id')->references('id')->on('aulas_questoes_alternativas')->onDelete('cascade');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('cadastro_questao_resposta');
    }
}
