<?php

use Illuminate\Database\Seeder;

class ContatoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('contato')->insert([
            'email' => 'brasil.faleconosco@roche.com',
            'telefone' => '11 2345 6789',
            'endereco' => 'Rua do Endereço Completo, 123, cj 222<br>Vila do Bairro - Cidade - UF - 01234-000',
            'informacoes_rodape' => 'Envie sua mensagem: <strong>brasil.faleconosco@roche.com</strong><br>Canal do Médico: <strong>0800 77 20 292</strong><br>Canal do Paciente: <strong>0800 77 20 289</strong><br>Horário de Atendimento: Segunda a Sexta-feira das 8h às 17h',
            'termos_rodape' => 'Caso opte pelo contato por e-mail, os dados pessoais informados serão registrados e armazenados em Sistema Informatizado para uso interno conforme legislação e procedimentos vigentes. Caso seja identificado em sua mensagem um relato de evento adverso, este poderá ser comunicado à Vigilância Sanitária. Caso não concorde com o registro de seus dados, por gentileza informe no texto de sua mensagem.',
        ]);
    }
}
