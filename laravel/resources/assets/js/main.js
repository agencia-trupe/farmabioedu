import AjaxSetup from './AjaxSetup';
import MobileToggle from './MobileToggle';

AjaxSetup();
MobileToggle();

if ($('.aulas .video-wrapper').length) {
    var iframe = document.querySelector('iframe');
    var player = new Vimeo.Player(iframe);

    var urlConcluir = $('.aulas .video-wrapper').data('concluir');

    player.on('ended', function() {
        $.ajax({ url: urlConcluir });
    });
}

if ($('.aula-quiz').length) {
    $('.aula-questao button').click(function() {
        var $botao   = $(this),
            $questao = $botao.parent(),
            url      = $questao.data('route'),
            selecao  = parseInt($questao.find('input:checked').val());

        if ($botao.hasClass('disabled')
            || $questao.hasClass('respondida')
            || ! selecao) return;

        $botao.addClass('disabled');

        $.post(url, {
            alternativa: selecao
        }, (data) => {
            if (data.erro) {
                alert(data.erro);
                return;
            }

            $questao.addClass('respondida');

            $questao.find('label').each((index, el) => {
                let $alternativa = $(el).find('input');
                let alternativaId = parseInt($alternativa.val());

                $alternativa.attr('disabled', true);

                if (alternativaId == selecao) {
                    $(el).addClass('selecao');
                } else if (data.corretas.indexOf(alternativaId) != -1) {
                    $(el).addClass('correta');
                }
            });

            if (data.corretas.indexOf(data.selecao) != -1) {
                $botao.addClass('certo').html('VOCÊ ACERTOU');
            } else {
                $botao.addClass('errado').html('VOCÊ ERROU');
            }
        }).fail(() => {
            alert('Ocorreu um erro. Tente novamente.');
            $botao.removeClass('disabled');
        });
    });
}
