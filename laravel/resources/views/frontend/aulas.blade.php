@extends('frontend.common.template')

@section('content')

    <div class="main texto aulas">
        <div class="center">
            <div class="texto-conteudo">
                <h1>AULAS</h1>

                <div class="video-wrapper" data-concluir="{{ route('aulas.assistida', $aula->id) }}">
                    <iframe src="https://player.vimeo.com/video/{{ $aula->video }}" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                </div>

                <div class="aulas-descricao">
                    <div class="palestrante-imagem">
                        <img src="{{ asset('assets/img/palestrantes/'.$aula->palestrante->foto) }}" alt="">
                        <span>.{{ sprintf("%02d", $numero + 1) }}</span>
                    </div>
                    <div class="palestrante-texto">
                        <h2>{{ $aula->palestrante->nome }}</h2>
                        <h3>{{ $aula->titulo }}</h3>
                        <p>{!! $aula->descricao !!}</p>
                    </div>

                    @if(count($aula->questoes))
                    <div class="aula-quiz">
                        <h4>TESTE SEUS CONHECIMENTOS</h4>

                        @foreach($aula->questoes as $key => $questao)
                        <?php
                            $resposta = $user->respostaQuestao($questao->id);
                        ?>
                        <div class="aula-questao @if($resposta) respondida @endif" data-route="{{ route('aulas.questaoPost', [$aula->id, $questao->id]) }}">
                            <span>.{{ sprintf("%02d", $key + 1) }}</span>
                            <p>{{ $questao->questao }}</p>
                            @foreach($questao->alternativas as $alternativa)
                            <label class="@if($resposta && $resposta->id == $alternativa->id) selecao @endif @if($resposta && $alternativa->alternativa_correta) correta @endif">
                                <input type="radio" name="alternativa-{{$questao->id}}" value="{{ $alternativa->id }}" @if($resposta) disabled @endif>
                                <span class="custom-radio"></span>
                                {{ $alternativa->alternativa }}
                            </label>
                            @endforeach
                            @if(!$resposta)
                            <button>RESPONDER</button>
                            @elseif($user->questaoCerta($questao->id))
                            <button class="certo">VOCÊ ACERTOU</button>
                            @else
                            <button class="errado">VOCÊ ERROU</button>
                            @endif
                        </div>
                        @endforeach
                    </div>
                    @endif
                </div>
            </div>

            <div class="aulas-lista">
                @foreach($aulas as $key => $a)
                <a href="{{ route('aulas', $a->slug) }}" @if($a->id == $aula->id) class="active" @endif>
                    <div class="palestrante-imagem">
                        <img src="{{ asset('assets/img/palestrantes/'.$a->palestrante->foto) }}" alt="">
                        <span>.{{ sprintf("%02d", $key + 1) }}</span>
                    </div>
                    <div class="palestrante-texto">
                        <h4>{{ $a->palestrante->nome }}</h4>
                        <h5>{{ $a->titulo }}</h5>
                    </div>
                </a>
                @endforeach
            </div>
        </div>
    </div>

@endsection
