@extends('frontend.common.template')

@section('content')

    <div class="main auth">
        <div class="banner">
            <div class="center">
                <div class="wrapper">
                    <h2>PRIMEIRO CADASTRO</h2>
                    <form action="{{ route('cadastroPost') }}" class="form-padrao form-cadastro" method="POST">
                        @if($errors->any())
                            <div class="erro">
                                @foreach($errors->all() as $error)
                                {{ $error }}<br>
                                @endforeach
                            </div>
                        @endif

                        {!! csrf_field() !!}

                        <div class="row">
                            <label for="nome">NOME</label>
                            <input type="text" name="nome" id="nome" value="{{ old('nome') }}" required>
                        </div>
                        <div class="row">
                            <label for="email">E-MAIL</label>
                            <input type="email" name="email" id="email" value="{{ old('email') }}" required>
                        </div>
                        <div class="row">
                            <label for="registro_profissional">REGISTRO PROF.</label>
                            <input type="text" name="registro_profissional" id="registro_profissional" value="{{ old('registro_profissional') }}" required>
                        </div>
                        <div class="row">
                            <label for="profissao">PROFISSÃO</label>
                            <input type="text" name="profissao" id="profissao" value="{{ old('profissao') }}" required>
                        </div>
                        <div class="row">
                            <label for="senha">SENHA</label>
                            <input type="password" name="senha" id="senha" required>
                        </div>
                        <div class="row">
                            <label for="senha_confirmation">REPETIR SENHA</label>
                            <input type="password" name="senha_confirmation" id="senha_confirmation" required>
                        </div>

                        <input type="submit" value="CADASTRAR">
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
