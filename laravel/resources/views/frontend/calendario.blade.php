@extends('frontend.common.template')

@section('content')

    <div class="main texto calendario">
        <div class="center">
            <div class="texto-icone">
                <img src="{{ asset('assets/img/layout/ilustra-calendario.png') }}" alt="">
            </div>

            <div class="texto-conteudo">
                <h1>CALENDÁRIO DE CURSOS</h1>

                @foreach($cursos as $curso)
                <div class="calendario-curso">
                    <h3>
                        {{ $curso->data->formatLocalized('%d %b %y') }}
                        -
                        {{ $curso->titulo }}
                    </h3>
                    <p>{!! $curso->informacoes !!}</p>
                </div>
                @endforeach
            </div>
        </div>
    </div>

@endsection
