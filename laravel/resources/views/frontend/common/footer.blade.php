    <footer>
        <div class="center">
            <div class="wrapper">
                <div class="informacoes">
                    <img src="{{ asset('assets/img/layout/marca-farmabioedu-rodape.png') }}" alt="">
                    <p>{!! $contato->informacoes_rodape !!}</p>
                </div>
                <div class="links">
                    <a href="{{ route('home') }}">_ home</a>
                    <a href="{{ route('sobre') }}">_ sobre o programa</a>
                    <a href="{{ route('aulas') }}">_ aulas</a>
                    <a href="{{ route('palestrantes') }}">_ palestrantes</a>
                    <a href="{{ route('calendario') }}">_ calendário de cursos</a>
                    <a href="{{ route('conselhos') }}">_ conselhos regionais</a>
                </div>
                <div class="links">
                    <a href="{{ route('contato') }}">_ contato</a>
                    <a href="{{ route('links') }}">_ links úteis</a>
                    <a href="{{ route('referencias') }}">_ referências bibliográficas</a>
                    <a href="{{ route('glossario') }}">_ glossário</a>
                    <a href="{{ route('politica') }}">_ política de privacidade</a>
                </div>
            </div>

            <div class="termos">
                <p>{!! $contato->termos_rodape !!}</p>
            </div>

            <div class="copyright">
                &copy; {{ date('Y') }} {{ config('app.name') }} - Todos os direitos reservados Segmento Farma
                <span>|</span>
                <a href="http://www.trupe.net" target="_blank">Criação de Sites</a>:
                <a href="http://www.trupe.net" target="_blank">Trupe Agência Criativa</a>
            </div>
        </div>
    </footer>
