    <header>
        <div class="center">
            <div class="wrapper">
                <a href="{{ route('home') }}" class="logo">
                    {{ config('app.name') }}
                    <img src="{{ asset('assets/img/layout/marca-farmabioedu-cabec.png') }}" alt="">
                </a>

                @if(auth('cadastro')->check())
                <a href="{{ route('dados') }}" class="btn-area-restrita @if(Tools::routeIs('dados')) active @endif">
                    [ÁREA RESTRITA]
                </a>
                @else
                <a href="{{ route('loginCadastro') }}" class="btn-area-restrita @if(Tools::routeIs(['loginCadastro', 'cadastro', 'esqueci', 'redefinirForm'])) active @endif">
                    [ÁREA RESTRITA]
                </a>
                @endif

                <button id="mobile-toggle" type="button" role="button">
                    <span class="lines"></span>
                </button>
            </div>

            <nav id="nav-desktop">
                <a href="{{ route('home') }}" @if(Tools::routeIs('home')) class="active" @endif>
                    <span class="icone home"></span>
                    <span class="nome">HOME</span>
                </a>
                <a href="{{ route('sobre') }}" @if(Tools::routeIs('sobre')) class="active" @endif>
                    <span class="icone sobre"></span>
                    <span class="nome">SOBRE O<br>PROGRAMA</span>
                </a>
                <a href="{{ route('aulas') }}" @if(Tools::routeIs('aulas')) class="active" @endif>
                    <span class="icone aulas"></span>
                    <span class="nome">AULAS</span>
                </a>
                <a href="{{ route('palestrantes') }}" @if(Tools::routeIs('palestrantes')) class="active" @endif>
                    <span class="icone palestrantes"></span>
                    <span class="nome">PALESTRANTES</span>
                </a>
                <a href="{{ route('calendario') }}" @if(Tools::routeIs('calendario')) class="active" @endif>
                    <span class="icone calendario"></span>
                    <span class="nome">CALENDÁRIO<br>DE CURSOS</span>
                </a>
                <a href="{{ route('conselhos') }}" @if(Tools::routeIs('conselhos')) class="active" @endif>
                    <span class="icone conselhos"></span>
                    <span class="nome">CONSELHOS<br>REGIONAIS</span>
                </a>
            </nav>
        </div>

        <div id="nav-mobile">
            <a href="{{ route('home') }}" @if(Tools::routeIs('home')) class="active" @endif>
                HOME
            </a>
            <a href="{{ route('sobre') }}" @if(Tools::routeIs('sobre')) class="active" @endif>
                SOBRE O PROGRAMA
            </a>
            <a href="{{ route('aulas') }}" @if(Tools::routeIs('aulas')) class="active" @endif>
                AULAS
            </a>
            <a href="{{ route('palestrantes') }}" @if(Tools::routeIs('palestrantes')) class="active" @endif>
                PALESTRANTES
            </a>
            <a href="{{ route('calendario') }}" @if(Tools::routeIs('calendario')) class="active" @endif>
                CALENDÁRIO DE CURSOS
            </a>
            <a href="{{ route('conselhos') }}" @if(Tools::routeIs('conselhos')) class="active" @endif>
                CONSELHOS REGIONAIS
            </a>
            <a href="{{ route('contato') }}" @if(Tools::routeIs('contato')) class="active" @endif>
                CONTATO
            </a>
            <a href="{{ route('links') }}" @if(Tools::routeIs('links')) class="active" @endif>
                LINKS ÚTEIS
            </a>
            <a href="{{ route('referencias') }}" @if(Tools::routeIs('referencias')) class="active" @endif>
                REFERÊNCIAS BIBLIOGRÁFICAS
            </a>
            <a href="{{ route('glossario') }}" @if(Tools::routeIs('glossario')) class="active" @endif>
                GLOSSÁRIO
            </a>
            <a href="{{ route('politica') }}" @if(Tools::routeIs('politica')) class="active" @endif>
                POLÍTICA DE PRIVACIDADE
            </a>
            @if(auth('cadastro')->check())
            <a href="{{ route('dados') }}" @if(Tools::routeIs('dados')) class="active" @endif>
                [ÁREA RESTRITA]
            </a>
            @else
            <a href="{{ route('loginCadastro') }}" @if(Tools::routeIs(['loginCadastro', 'cadastro', 'esqueci', 'redefinirForm'])) class="active" @endif>
                [ÁREA RESTRITA]
            </a>
            @endif
        </div>
    </header>
