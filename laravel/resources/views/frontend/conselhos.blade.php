@extends('frontend.common.template')

@section('content')

    <div class="main texto conselhos">
        <div class="center">
            <div class="texto-icone">
                <img src="{{ asset('assets/img/layout/ilustra-conselhos.png') }}" alt="">
            </div>

            <div class="texto-conteudo">
                <h1>CONSELHOS REGIONAIS</h1>

                @foreach($conselhos as $conselho)
                <div class="conselhos-conselho">
                    <h3>{{ $conselho->titulo }}</h3>
                    <p>{!! $conselho->informacoes !!}</p>
                </div>
                @endforeach
            </div>
        </div>
    </div>

@endsection
