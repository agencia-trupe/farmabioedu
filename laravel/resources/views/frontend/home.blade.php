@extends('frontend.common.template')

@section('content')

    <div class="main home">
        <div class="banner">
            <div class="center">
                <div class="banner-frase">
                    Uma iniciativa inédita,<br>que leva informações sobre medicamentos biológicos para o universo farmacêutico
                </div>
            </div>
        </div>

        <div class="center">
            <div class="home-frase"></div>

            <div class="home-palestrantes">
                @foreach($palestrantes as $palestrante)
                <div class="palestrante">
                    <img src="{{ asset('assets/img/palestrantes/'.$palestrante->foto) }}" alt="">
                    <span class="nome">{{ $palestrante->nome }}</span>
                    <span class="apresentacao">{{ $palestrante->apresentacao }}</span>
                </div>
                @endforeach
            </div>
        </div>
    </div>

@endsection
