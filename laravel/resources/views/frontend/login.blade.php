@extends('frontend.common.template')

@section('content')

    <div class="main auth">
        <div class="banner">
            <div class="center">
                <div class="wrapper">
                    <h2>LOGIN</h2>
                    <form action="{{ route('loginCadastroPost') }}" class="form-login" method="POST">
                        @if(session('erro-login'))
                            <div class="erro">{{ session('erro-login') }}</div>
                        @endif

                        {!! csrf_field() !!}

                        <div class="form-login-content">
                            <div class="fields">
                                <input type="email" name="email" value="{{ old('email') }}" placeholder="e-mail" required>
                                <input type="password" name="senha" placeholder="senha" required>
                            </div>
                            <input type="submit" value="ENTRAR">
                        </div>

                        <a href="{{ route('esqueci') }}" class="btn-esqueci">esqueci minha senha &raquo;</a>
                    </form>

                    <a href="{{ route('cadastro') }}" class="btn-cadastro">
                        AINDA NÃO TENHO CADASTRO > CADASTRAR
                    </a>
                </div>
            </div>
        </div>
    </div>

@endsection
