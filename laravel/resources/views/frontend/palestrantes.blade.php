@extends('frontend.common.template')

@section('content')

    <div class="main texto palestrantes">
        <div class="center">
            <div class="texto-icone">
                <img src="{{ asset('assets/img/layout/ilustra-palestrantes.png') }}" alt="">
            </div>

            <div class="texto-conteudo">
                <h1>PALESTRANTES</h1>

                @foreach($palestrantes as $palestrante)
                <div class="palestrante">
                    <img src="{{ asset('assets/img/palestrantes/'.$palestrante->foto) }}" alt="">
                    <div class="palestrante-texto">
                        <h3>{{ $palestrante->nome }}</h3>
                        <h4>{{ $palestrante->apresentacao }}</h4>
                        <p>{!! $palestrante->curriculo !!}</p>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>

@endsection
