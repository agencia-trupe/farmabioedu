@extends('frontend.common.template')

@section('content')

    <div class="main auth">
        <div class="banner">
            <div class="center">
                <div class="wrapper">
                    <h2>REDEFINIÇÃO DE SENHA</h2>

                    <form action="{{ route('redefinir') }}" class="form-padrao form-cadastro" method="POST">
                        {!! csrf_field() !!}

                        @if($errors->any())
                            <div class="erro">
                                @foreach($errors->all() as $error)
                                {{ $error }}<br>
                                @endforeach
                            </div>
                        @endif

                        <input type="hidden" name="token" value="{{ $token }}">
                        <input type="hidden" name="email" value="{{ $email }}" required>

                        <div class="row">
                            <label for="senha">SENHA</label>
                            <input type="password" name="password" id="senha" required>
                        </div>
                        <div class="row">
                            <label for="senha_confirmation">REPETIR SENHA</label>
                            <input type="password" name="password_confirmation" id="senha_confirmation" required>
                        </div>

                        <input type="submit" value="REDEFINIR">
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
