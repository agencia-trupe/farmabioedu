@extends('frontend.common.template')

@section('content')

    <div class="main texto">
        <div class="center">
            @if($texto->icone)
            <div class="texto-icone">
                <img src="{{ asset('assets/img/layout/ilustra-'.$texto->icone.'.png') }}" alt="">
            </div>
            @endif

            <div class="texto-conteudo">
                <h1>{{ $texto->titulo }}</h1>
                {!! $texto->texto !!}

                @if($texto->video)
                <div class="video-wrapper">
                    <iframe src="https://player.vimeo.com/video/{{ $texto->video }}" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                </div>
                @endif
            </div>
        </div>
    </div>

@endsection
