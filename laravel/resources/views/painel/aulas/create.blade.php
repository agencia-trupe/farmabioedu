@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Aulas /</small> Adicionar Aula</h2>
    </legend>

    {!! Form::open(['route' => 'painel.aulas.store', 'files' => true]) !!}

        @include('painel.aulas.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
