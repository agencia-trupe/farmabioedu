@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <legend>
        <h2>
            Cadastros
        </h2>
    </legend>

    @if(!count($registros))
    <div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
    @else
    <a href="{{ route('painel.cadastros.exportar') }}" class="btn btn-md btn-info" style="margin-bottom:25px">
        <span class="glyphicon glyphicon-th" style="margin-right:10px"></span>
        Exportar XLS
    </a>
    <table class="table table-striped table-bordered table-hover table-info">
        <thead>
            <tr>
                <th>Nome</th>
                <th>Aulas Concluídas</th>
                <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)
            <tr class="tr-row" id="{{ $registro->id }}">
                <td>{{ $registro->nome }}</td>
                <td>
                    {{ $registro->fracaoAulasConcluidas }}
                    <span class="label label-{{ $registro->todasAulasConcluidas() ?'success' : 'default' }}" style="margin-left:.5em;font-size:1em">{{ $registro->porcentagemAulasConcluidas }}%</span>
                    @if($registro->aptoAoCertificado())
                    <span class="label label-success" style="margin-left:.5em;font-size:1em">Apto ao certificado</span>
                    @endif
                </td>
                <td class="crud-actions">
                    <a href="{{ route('painel.cadastros.show', $registro->id ) }}" class="btn btn-primary btn-sm pull-left">
                        <span class="glyphicon glyphicon-align-left" style="margin-right:10px;"></span>Ver cadastro
                    </a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    {!! $registros->render() !!}
    @endif

@endsection
