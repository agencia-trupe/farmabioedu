@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Cadastros /</small> {{ $registro->nome }}</h2>
    </legend>

    <div class="form-group">
        <label>Nome</label>
        <div class="well">{{ $registro->nome }}</div>
    </div>

    <div class="form-group">
        <label>E-mail</label>
        <div class="well">
            <button class="btn btn-info btn-sm clipboard" data-clipboard-text="{{ $registro->email }}" style="margin-right:5px;border:0;transition:background .3s">
                <span class="glyphicon glyphicon-copy"></span>
            </button>
            {{ $registro->email }}
        </div>
    </div>

    <div class="form-group">
        <label>Registro Profissional</label>
        <div class="well">{{ $registro->registro_profissional }}</div>
    </div>

    <div class="form-group">
        <label>Profissão</label>
        <div class="well">{{ $registro->profissao }}</div>
    </div>

    <div class="form-group">
        <label>Aulas Concluídas</label>
        <div class="well">
            {{ $registro->fracaoAulasConcluidas }}
            <span class="label label-{{ $registro->todasAulasConcluidas() ?'success' : 'default' }}" style="margin-left:.5em;font-size:1em">{{ $registro->porcentagemAulasConcluidas }}%</span>
            @if($registro->aptoAoCertificado())
            <span class="label label-success" style="margin-left:.5em;font-size:1em">Apto ao certificado</span>
            @endif
            @if(count($aulas))
            <hr style="border-color:#ccc">
            @foreach($aulas as $video)
                <p style="margin:.5em 0 0;">
                    <span class="glyphicon glyphicon-{{ $registro->aulas->contains($video->id) ? 'ok' : 'remove' }}" style="margin-right:.5em"></span>
                    {{ $video->titulo }}
                    [{{ $registro->acertosPorAula($video->id) }} acerto(s) de {{ $video->questoes->count() }} questões]
                </p>
            @endforeach
            @endif
        </div>
    </div>

    <a href="{{ route('painel.cadastros.index') }}" class="btn btn-default btn-voltar">Voltar</a>

@stop
