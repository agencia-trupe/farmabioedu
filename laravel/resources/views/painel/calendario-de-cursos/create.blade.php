@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Calendário de Cursos /</small> Adicionar Curso</h2>
    </legend>

    {!! Form::open(['route' => 'painel.calendario-de-cursos.store', 'files' => true]) !!}

        @include('painel.calendario-de-cursos.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
