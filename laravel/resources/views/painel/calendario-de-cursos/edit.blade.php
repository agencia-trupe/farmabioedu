@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Calendário de Cursos /</small> Editar Curso</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.calendario-de-cursos.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.calendario-de-cursos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
