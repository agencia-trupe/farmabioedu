@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('data', 'Data') !!}
    {!! Form::text('data', isset($registro) && $registro->data ? $registro->data->format('d/m/Y') : null, ['class' => 'form-control datepicker']) !!}
</div>

<div class="form-group">
    {!! Form::label('titulo', 'Título') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('informacoes', 'Informações') !!}
    {!! Form::textarea('informacoes', null, ['class' => 'form-control ckeditor', 'data-editor' => 'conselho']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.calendario-de-cursos.index') }}" class="btn btn-default btn-voltar">Voltar</a>
