<ul class="nav navbar-nav">
    <li class="dropdown @if(Tools::routeIs(['painel.sobre-o-programa*', 'painel.links-uteis*', 'painel.referencias-bibliograficas*', 'painel.glossario*', 'painel.politica-de-privacidade*'])) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Textos
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li @if(Tools::routeIs('painel.sobre-o-programa*')) class="active" @endif>
                <a href="{{ route('painel.sobre-o-programa.index') }}">Sobre o Programa</a>
            </li>
            <li @if(Tools::routeIs('painel.links-uteis*')) class="active" @endif>
                <a href="{{ route('painel.links-uteis.index') }}">Links Úteis</a>
            </li>
            <li @if(Tools::routeIs('painel.referencias-bibliograficas*')) class="active" @endif>
                <a href="{{ route('painel.referencias-bibliograficas.index') }}">Referências Bibliográficas</a>
            </li>
            <li @if(Tools::routeIs('painel.glossario*')) class="active" @endif>
                <a href="{{ route('painel.glossario.index') }}">Glossário</a>
            </li>
            <li @if(Tools::routeIs('painel.politica-de-privacidade*')) class="active" @endif>
                <a href="{{ route('painel.politica-de-privacidade.index') }}">Política de Privacidade</a>
            </li>
        </ul>
    </li>
    <li @if(Tools::routeIs('painel.aulas*')) class="active" @endif>
        <a href="{{ route('painel.aulas.index') }}">Aulas</a>
    </li>
    <li @if(Tools::routeIs('painel.palestrantes*')) class="active" @endif>
        <a href="{{ route('painel.palestrantes.index') }}">Palestrantes</a>
    </li>
    <li @if(Tools::routeIs('painel.calendario-de-cursos*')) class="active" @endif>
        <a href="{{ route('painel.calendario-de-cursos.index') }}">Calendário de Cursos</a>
    </li>
    <li @if(Tools::routeIs('painel.conselhos-regionais*')) class="active" @endif>
        <a href="{{ route('painel.conselhos-regionais.index') }}">Conselhos Regionais</a>
    </li>
    <li class="dropdown @if(Tools::routeIs('painel.contato*')) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Contato
            @if($contatosNaoLidos >= 1)
            <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
            @endif
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li @if(Tools::routeIs('painel.contato.index')) class="active" @endif>
                <a href="{{ route('painel.contato.index') }}">Informações de Contato</a>
            </li>
            <li @if(Tools::routeIs('painel.contato.recebidos*')) class="active" @endif>
                <a href="{{ route('painel.contato.recebidos.index') }}">
                    Contatos Recebidos
                    @if($contatosNaoLidos >= 1)
                    <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
                    @endif
                </a>
            </li>
        </ul>
    </li>
    <li @if(Tools::routeIs('painel.cadastros*')) class="active" @endif>
        <a href="{{ route('painel.cadastros.index') }}">Cadastros</a>
    </li>
</ul>
