@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Conselhos Regionais /</small> Adicionar Conselho</h2>
    </legend>

    {!! Form::open(['route' => 'painel.conselhos-regionais.store', 'files' => true]) !!}

        @include('painel.conselhos-regionais.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
