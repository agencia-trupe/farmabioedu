@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Conselhos Regionais /</small> Editar Conselho</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.conselhos-regionais.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.conselhos-regionais.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
