@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('email', 'E-mail (para recebimento de mensagens do formulário de contato)') !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('telefone', 'Telefone') !!}
    {!! Form::text('telefone', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('endereco', 'Endereço') !!}
    {!! Form::textarea('endereco', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
</div>

<div class="form-group">
    {!! Form::label('informacoes_rodape', 'Informações Rodapé') !!}
    {!! Form::textarea('informacoes_rodape', null, ['class' => 'form-control ckeditor', 'data-editor' => 'infoContato']) !!}
</div>

<div class="form-group">
    {!! Form::label('termos_rodape', 'Termos Rodapé') !!}
    {!! Form::textarea('termos_rodape', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
