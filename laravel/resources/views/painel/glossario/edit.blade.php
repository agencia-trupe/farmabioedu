@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Glossário</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.glossario.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.glossario.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
