@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Links Úteis</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.links-uteis.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.links-uteis.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
