@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('nome', 'Nome') !!}
    {!! Form::text('nome', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('apresentacao', 'Apresentação') !!}
    {!! Form::text('apresentacao', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('curriculo', 'Currículo') !!}
    {!! Form::textarea('curriculo', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
</div>

<div class="well form-group">
    {!! Form::label('foto', 'Foto') !!}
@if($submitText == 'Alterar')
    <img src="{{ url('assets/img/palestrantes/'.$registro->foto) }}" style="display:block; margin-bottom: 10px; max-width: 100%; border-radius: 100%">
@endif
    {!! Form::file('foto', ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.palestrantes.index') }}" class="btn btn-default btn-voltar">Voltar</a>
