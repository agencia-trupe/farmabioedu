@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Referências Bibliográficas</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.referencias-bibliograficas.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.referencias-bibliograficas.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
